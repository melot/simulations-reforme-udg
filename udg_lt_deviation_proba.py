# -*- coding: utf-8 -*-

"""
Génère une évolution de population de normaliens sur et leurs mises
réalisées au thurnage général, puis reproduit un grand nombre de fois
chaque TG pour déterminer les probabilités effectives d’obtention de thurne.
"""

import pandas as pd
import numpy as np
import random
import matplotlib.pyplot as plt
import hashlib
import scipy.stats

CHOCOLAT=0
BRONZE=1
ARGENT=2
OR=3
GL=4 #Pour simplifier les simulations, les GL sont placés dans une catégorie
     #supérieure
CATINDEF=-1
BRONZEARGENT=-2

TAILLE_PROMO_EFF = 270
NB_ELEVES_PROMO = 200
NB_ETUDIANTS_PROMO = 150
TAUX_GL = 0.25 #Les SI sont comptés comme GL pour simplifier

ELEVE = 0
ETUDIANT = 1


PATH = ""


def reset_seed():
    """
    Réinitialise les générateurs d’aléa. À appeler avant chaque relance du
    code.
    """
    KEY = "oxycoupage gouge acétylène carrières écopâturage pointeau".encode("utf8")
    m = hashlib.sha1()
    m.update(KEY)
    seed = int(m.hexdigest(), 16) % 2**32
    np.random.seed(seed)
    random.seed(seed)

# Évolution du nombre de thurnes disponibles en fonction des années.
# Chiffres générés aléatoirement à l’aide d’un code repris à Sayah :
#parc = Internat()
##l = []
##for i in range(200):
##    l.append(parc.thurnes_dispo())
##    parc.contexte()
NB_THURNES = [  371, 328, 330, 378, 418, 282, 313, 362, 323, 316,
                391, 317, 394, 353, 348, 316, 256, 290, 365, 319,
                372, 242, 317, 328, 415, 301, 217, 328, 262, 362,
                379, 366, 252, 344, 295, 313, 274, 339, 295, 267,
                438, 253, 184, 301, 343, 345, 357, 266, 346, 381,
                187, 258, 331, 306, 328, 365, 298, 219, 282, 317,
                280, 302, 370, 302, 261, 301, 260, 329, 312, 346,
                290, 219, 338, 289, 303, 321, 330, 223, 242, 237,
                334, 349, 406, 336, 265, 386, 358, 366, 292, 149,
                300, 228, 290, 314, 310, 360, 335, 365, 294, 233,
                336, 354, 357, 258, 162, 329, 307, 322, 223, 337,
                311, 280, 376, 354, 200, 208, 322, 329, 325, 258,
                366, 375, 279, 258, 314, 227, 288, 373, 237, 294,
                313, 390, 359, 300, 382, 289, 279, 204, 350, 347,
                285, 284, 310, 376, 366, 266, 365, 307, 314, 274,
                236, 278, 344, 324, 215, 289, 307, 304, 112, 326,
                315, 384, 309, 290, 255, 304, 310, 345, 301, 290,
                416, 303, 375, 396, 328, 343, 349, 407, 337, 341,
                330, 316, 338, 276, 196, 288, 318, 330, 391, 388,
                400, 403, 369, 366, 341, 335, 310, 223, 180, 208,
]


















#%% Première étape : on initialise une « vraie » trajectoire de la
# population normalienne.
# Chaque année, on fait donc un TG, dont les résultats déterminent le crédit
# d’UDG des normaliens en vue des années suivantes, etc.


def evol_promo(old_promo, annee, cible_udg_n, cible_udg_nplusun):
    """
    À partir d’une promotion donnée, en génère une nouvelle.

    Parameters
    ----------
    annee_n : TYPE
        DataFrame contenant les normaliens de l’année n.
    cible_udg_n : float
        la cible d’UDG pour l’année passée.
    cible_udg_nplusun : float
        la cible d’UDG pour la nouvelle année.

    Returns
    -------
        DataFrame dans lequel :
            * Les normaliens en fin de scolarité ont été retirés (avec une
              part d’aléa pour la durée de scolarité)
            * Des nouveaux normaliens, avec leurs statuts et leurs éventuelles
              garanties de logement ont été ajoutés
            * Le nombre d’UDG disponibles a été mis à jour
    """
    new_promo = old_promo.copy()
    new_promo["annee"] += 1
    new_promo = new_promo[new_promo["annee"] < new_promo["finscol"]] #Virer les vieux (inégalité stricte car fin d’année)
    
    new_promo["UDG_dispo"] *= (cible_udg_nplusun / cible_udg_n)
    
    global CURRENT_ID_NORMALIEN
    conscrits = pd.DataFrame({
        "id_normalien": range(CURRENT_ID_NORMALIEN, CURRENT_ID_NORMALIEN + NB_ELEVES_PROMO + NB_ETUDIANTS_PROMO),
        "statut": [ELEVE]*NB_ELEVES_PROMO + [ETUDIANT]*NB_ETUDIANTS_PROMO,
        "annee": 1,
        "GL": [False]*NB_ELEVES_PROMO + list(np.random.random(size=NB_ETUDIANTS_PROMO) < TAUX_GL),
        "finscol": [4]*NB_ELEVES_PROMO + [3]*NB_ETUDIANTS_PROMO,
        "promo": annee,
        "UDG_dispo": cible_udg_nplusun,
    }).set_index("id_normalien")
    CURRENT_ID_NORMALIEN += NB_ELEVES_PROMO + NB_ETUDIANTS_PROMO
    
    # Chacun a 50 % de chances de prendre une prolongation de scolarité,
    # et 20 % de chances d’en prendre deux.
    random_finscol = np.random.random(size=NB_ELEVES_PROMO + NB_ETUDIANTS_PROMO)
    conscrits["finscol"] += (random_finscol < 0.5)
    conscrits["finscol"] += (random_finscol < 0.2)
    
    return new_promo.append(conscrits)


def random_nonunif(vals, probs):
        """
        Tire un élément de vals aléatoirement, vals[i] ayant une proba probs[i] d’être tiré.

        Parameters
        ----------
        vals : array
        probs : array
        """
        cumprobs = np.concatenate([[0], np.cumsum(probs)])
        cumprobs = cumprobs / cumprobs[-1]
        assert np.isclose(cumprobs[-1], 1)
        r = np.random.random()
        return vals[max(i for i, v in enumerate(cumprobs) if v <= r)]

def truncdec(x):
    return x // 0.1 / 10
    
def mises_TG(promo, cible_udg):
    """
    Définit les mises des membres d’une promo.
    Les probabilités de jeu sont une hypothèse du modèle.
    Parameters
    ----------
    promo : DataFrame
        La promotion concernée.
    cible_udg: float
        La cible d’UDG pour l’année.
    Returns
    -------
    None. Le DataFrame est modifié pour ajouter les mises.

    """
        
    promo["mise"] = 0
    promo["firsttime"] = False
    for index, row in promo.iterrows():
        mise = 0
        mise_max = row["UDG_dispo"] #Tronquer la mise max possible à deux chiffres après les unités

        firsttime = (mise_max > cible_udg - .01)
        
        if row["annee"] == 1 and cible_udg > 1:
            # Conscrit une année où la cible est > 1
            prob_participe = .6
            m = [ .1,  .2,  .3,  .4,   5, .6,  .7,  .8,  .9,   1,  mise_max]
            p = [.05, .05, .10, .15, .15, .1, .05, .05, .05,  .2,       .05]
        
        elif row["annee"] == 1:
            # Conscrit une année pourrie
            prob_participe = .45 #On suppose une tendance à faire des économies
            decilemax = truncdec(cible_udg)
            demi = truncdec(cible_udg / 2)
            m = [.1,  .2, demi, decilemax - .1, decilemax, mise_max]
            p = [.1,  .1,   .3,             .1,        .1,       .1]
            autresmises = list(x for x in np.arange(0, decilemax, .1) if x not in m)
            if l := len(autresmises) > 0:
                m = m + autresmises
                autresprobs = [0.2 / l] * l
                p = p + autresprobs
            else:
                p[0] += .2
        
        elif row["annee"] > 1 and not firsttime:
            # Vieux ayant déjà participé à un thurnage
            prob_participe = .5
            if row["annee"] == row["finscol"] - 1:
                # Dernière année
                m = [mise_max]
                p = [1]
            else:
                # Autres
                m = list(np.arange(0, mise_max, 0.1)) + [mise_max]
                p = [1 / len(m)] * len(m)
            
        else:
            # Vieux n’ayant jamais participé à un thurnage
            prob_participe = .15
            if row["annee"] == row["finscol"] - 1:
                #Dernière année
                m = [mise_max]
                p = [1]
            else:
                #Autres
                m = [.1,  .2,  .3,  .4,  .5,  .6,  .7,  .8,  .9,   1, mise_max]
                p = [.1, .05, .05, .05, .05, .05, .05, .05, .05, .15,       .2]
        
        if np.random.random() < prob_participe:
            mise = random_nonunif(m, p)
        else:
            mise = 0
            
        mise = min(mise, mise_max) #Écréter les mises
        mise = max(mise, 0) #On ne sait jamais…
        promo.loc[index, "mise"] = mise
        if mise > 0 and firsttime:
            promo.loc[index, "firsttime"] = True




# Simulation d’un thurnage général donné
# Pour simplifier, on considère que toute personne qui ne s’est pas inscrite
# est inscrite avec 0 UDG. En particulier, les GL prennent une thurne
# chaque année

def liste_thurnabilite(liste_mises, nb_thurnes):
    """
    Parameters
    ----------
    liste_normaliens : dataFrame
        dataFrame comprenant au moins un champ "mise" correspondant à la
        mise de chaque normalien, et un champ "GL" booléen
        Un champ "id_normalien" doit correspondre à l’identifiant unique de
        chacun pour le thurnage. Ce champ ne doit pas être l’index.
        Un normalien est une ligne
    nb_thurnes : int
        nombre de thurnes mises en jeu

    Returns
    -------
    dataFrame
        dataFrame auquel est rajouté un champ "thurnable", un champ
        "categorie" et un chamb "classement" (de thurnabilité, pas d’appel)
        Les GL sont prises en compte. Les normaliens bénéficiant d’une GL sont,
        pour simplifier, considérés comme thurnables dans tous les cas

    """
    # Ne pas tenir compte des boost
    res = liste_mises
    res.loc[res["mise"] >= 1, "mise"] = 1
    # Nb : en l’état, on ne décompte pas non plus les mises au-dessus de 1. Cela est plutôt défavorable à notre modèle.
    
    res["categorie"] = CATINDEF
    res.loc[res["mise"] < .01, "categorie"] = CHOCOLAT
    res.loc[(res["mise"] >= .01)
            & (res["mise"] <= .99), "categorie"] = BRONZEARGENT
    res.loc[res["mise"] > .99, "categorie"] = OR
    res.loc[res["GL"], "categorie"] = GL
    # Nota : on fait comme si les GL étaient mieux classés que les OR, ce qui
    # est sans influence sur ce modèle puisque l’on n’attribue pas les thurnes.
    # Tous les GL se voient donc débiter leur mise quand ils en font une, mais cela
    # est donc en pratique sans conséquence.
    
    # Tirages au sort, affectation par catégorie et détermination du classement
    # pour chaque thurnage
    res["randomclassement"]= np.random.random(size=len(res))
    liste_argent = (res["categorie"] == BRONZEARGENT) & (res["randomclassement"] < res["mise"])
    res.loc[liste_argent, "categorie"] = ARGENT
    res.loc[res["categorie"] == BRONZEARGENT, "categorie"] = BRONZE
    res.sort_values(["categorie", "mise", "randomclassement"],
                    ascending=[False, False, True],
                    inplace=True)
    res["classement"] = range(len(res))
    
    res["thurnable"] = res["classement"] < nb_thurnes
    
    res.drop(["randomclassement"], axis="columns", inplace=True)



reset_seed()


CURRENT_ID_NORMALIEN = 0
CIBLE_2021 = 1.33

liste_promos = []
cibles_udg = [] #ne pas mettre la cible 2021, car la promo 0 n’est pas comptée
#new_promo = pd.DataFrame(promo0).set_index("id_normalien")

#Partir d’une promotion vide. Les dix premières années d’évolution seront donc abandonnées.
promo = pd.DataFrame(columns=["id_normalien", "statut", "annee", "UDG_dispo", "finscol", "GL", "promo"]).set_index("id_normalien")
mises_TG(promo, CIBLE_2021)

cible_udg = CIBLE_2021

for y, taille_parc in enumerate(NB_THURNES):
    print("Année", y)
    new_cible_udg = taille_parc / TAILLE_PROMO_EFF
    promo = evol_promo(promo, y, cible_udg, new_cible_udg) #Générer une nouvelle année
    cible_udg = new_cible_udg
    cibles_udg.append(cible_udg)
    
    #Organiser le TG
    mises_TG(promo, cible_udg)
    liste_thurnabilite(promo, taille_parc)
    
    # Décompter les mises et conserver une copie des archives
    promo["UDG_dispo"] -= promo["mise"] * promo["thurnable"]
    liste_promos.append(promo)
    
#Compte tenu du caractère partiel des données d’initialisation, supprimer les
#premières années.

DUREE_INIT = 10

liste_promos = liste_promos[DUREE_INIT:]
cibles_udg = cibles_udg[DUREE_INIT:]
NB_THURNES = NB_THURNES[DUREE_INIT:]



#%% Statistiques et données de contrôle sur la première partie

def stats_un_TG(promo):
    """
    Réalise des statistiques sur le taux de thurnabilité par catégorie sur
    un ensemble de simulations de thurnages.
    
    Parameters
    ----------
    liste_thurnabilite : DataFrame
        dataFrame de promotion comportant les résultats de thurnage
        
    Returns
    -------
        Un dictionnaire contenant un certain nombre d’indicateurs sur le TG
        de l’année considérée.

    """
    ls = promo
    stats = {}
    
    champs_cat = ["%Or logés", "%Argent logés", "%Bronze logés", "%Chocolat logés"]
    for cat, i in enumerate([OR, ARGENT, BRONZE, CHOCOLAT]):
        totcat = sum(ls["categorie"] == cat)
        if totcat == 0:
            stats[champs_cat[i]] = None
        else:
            stats[champs_cat[i]] = sum((ls["categorie"] == cat) & (ls["thurnable"])) / totcat
                            
    stats["nb GL"] = sum(ls["categorie"] == GL)
    stats["nb inscrits"] = sum(ls["categorie"] != CHOCOLAT) #On amalgame jouer 0 et ne pas jouer
    stats["nb 1 UDG"] = sum(ls["mise"] >= 1)
    stats["nb_normaliens"] = len(ls)
    stats["nb firsttime"] = sum(ls["firsttime"])
    stats["nb conscrits jouent"] = sum((ls["annee"] == 1) & ((ls["mise"] > 0) | ls["GL"]))
    stats["nb UDG quart1"] = sum((ls["mise"] < .25) & (ls["mise"] > 0))
    stats["nb UDG quart2"] = sum((ls["mise"] < .5) & (ls["mise"] > .25))
    stats["nb UDG quart3"] = sum((ls["mise"] < .75) & (ls["mise"] > .5))
    stats["nb UDG quart4"] = sum((ls["mise"] < 1) & (ls["mise"] > .75))
    stats["nb thurnables"] = sum(ls["thurnable"])
    #Liste des thurnables qui ne sont pas des GL de catégorie différente de or, pour calculer la marge d’or
    stats["nb thurnables hors GL non or"] = sum(ls["thurnable"] & ~(ls["GL"] & (ls["mise"] < 1)))
    
    return stats

stats1 = pd.DataFrame([(stats_un_TG(p)) for p in liste_promos])
stats1["cible UDG"] = cibles_udg
stats1["nb thurnes"] = NB_THURNES
stats1["% inscrits 1 UDG"] = stats1["nb 1 UDG"] / stats1["nb inscrits"]
stats1["% UDG quart1"] = stats1["nb UDG quart1"] / stats1["nb inscrits"]
stats1["% UDG quart2"] = stats1["nb UDG quart2"] / stats1["nb inscrits"]
stats1["% UDG quart3"] = stats1["nb UDG quart3"] / stats1["nb inscrits"]
stats1["% UDG quart4"] = stats1["nb UDG quart4"] / stats1["nb inscrits"]
stats1["marge or"] = stats1["nb thurnables hors GL non or"] / stats1["nb 1 UDG"]

# Variables de contrôle : permet d’assurer que les mises modélisées sont
# cohérentes avec ce qui est observé.
# On s’attend à avoir :
    # nb_inscrits autour de 400 à 500
    # nb firsttime autour de 270 (sauf si cible_UDG < 1)
    # nb conscrits jouent autour de 220 (idem)
    # % UDG quart1 très variables, mais sans forcément de disparition complète
    # % inscrits 1 UDG entre 0.2 et 0.35 selon les années
controle = stats1[["nb inscrits", "nb firsttime", "nb conscrits jouent",
                   "% UDG quart1", "% UDG quart2", "% UDG quart3", "% UDG quart4", "% inscrits 1 UDG"]]

print(controle)





#%% Deuxième étape : une fois cela fait, regénérer, pour chaque thurnage,
# dix mille thurnages équivalents afin de calculer les probabilités
# empiriques de thurnage par personne
#
# liste_promos, issu de la première partie, doit obligatoirement être
# généré

def liste_thurnabilite_multi(liste_mises, nb_thurnes, nb_thurnages):
    """
    Parameters
    ----------
    liste_normaliens : dataFrame
        dataFrame comprenant au moins un champ "mise" correspondant à la
        mise de chaque normalien, et un champ "GL" booléen
        Un champ "id_normalien" doit correspondre à l’identifiant unique de
        chacun pour le thurnage. Ce champ ne doit pas être l’index.
        Un normalien est une ligne
    nb_thurnes : int
        nombre de thurnes mises en jeu
    nb_thurnages : int, optional
        si différent de 1, n thurnages différents seront traités en parallèle
        (vectorisés)

    Returns
    -------
    dataFrame
        dataFrame auquel est rajouté un champ "thurnable", un champ
        "categorie" et un chamb "classement" (de thurnabilité, pas d’appel)
        si n > 1, un champ id_thurnage est rajouté. Le classement et la
        catégorie sont relatifs à chaque thurnage. Chaque normalien doit
        être porteur d’un index qui est conservé lors des copies si le but
        est de le réidentifier par la suite.
        Les GL sont prises en compte. Les normaliens bénéficiant d’une GL sont,
        pour simplifier, considérés comme thurnables dans tous les cas

    """
    # Ne pas tenir compte des boost
    
    res = liste_mises.copy()
    res = res.reset_index()
    res.loc[res["mise"] >= 1, "mise"] = 1
    
    nb_normaliens = len(liste_mises)
    res["categorie"] = CATINDEF
    res.loc[res["mise"] < .01, "categorie"] = CHOCOLAT
    res.loc[(res["mise"] >= .01)
            & (res["mise"] <= .99), "categorie"] = BRONZEARGENT
    res.loc[res["mise"] > .99, "categorie"] = OR
    res.loc[res["GL"], "categorie"] = GL
    # Nota : on fait comme si les GL étaient mieux classés que les OR, ce qui
    # est sans influence sur ce modèle puisque l’on n’attribue pas les thurnes.
    # Tous les GL se voient donc débiter leur mise quand ils en font une, mais cela
    # est donc en pratique sans conséquence.
    
    #Dupliquer la liste en fonction du nombre de thurnages doivent être faits
    nums_thurnage = pd.DataFrame({"id_thurnage": range(nb_thurnages)})
    res = res.merge(nums_thurnage, how="cross")
    
    # Tirages au sort, affectation par catégorie et détermination du classement
    # pour chaque thurnage
    res["randomclassement"]= np.random.random(size=len(res))
    liste_argent = (res["categorie"] == BRONZEARGENT) & (res["randomclassement"] < res["mise"])
    res.loc[liste_argent, "categorie"] = ARGENT
    res.loc[res["categorie"] == BRONZEARGENT, "categorie"] = BRONZE
    res.sort_values(["id_thurnage", "categorie", "mise", "randomclassement"],
                    ascending=[True, False, False, True],
                    inplace=True)
    res["classement"] = range(len(res))
    res["classement"] %= nb_normaliens
    
    res["thurnable"] = res["classement"] < nb_thurnes
    
    res.drop(["randomclassement"], axis="columns", inplace=True)
    res = res.reset_index()
    res.drop(["index"], axis="columns", inplace=True)
    
    return res


def pemp_thurnabilite(listes_thurnabilite):
    """
    Calcule la probabilité empirique d’être thurnable sur un grand nombre de
    simulations d’un thurnage, données par la fonction liste_thurnabilite.
    Parameters
    ----------
    class_thurnage: dataFrame
        dataFrame de résultats de thurnages générés par la fonction
        classement_thurnage.
        Chaque ligne doit correspondre à un normalien dans un des thurnages
        générés.
        La colonne id_thurnage doit exister.

    Returns
    -------
        Un pd.DataFrame contenant :
            * id_normalien : l’identifiant original de chaque normalien sans
              GL ayant misé entre 0 et 1 UDG (strictement)
            * mise : la mise jouée (donnée initialement)
            * pemp : la probabilité empirique d’être sur liste de thurnabilité
              calculée sur un grand nombre de simulations (lors de l’appel
              de la fonction liste_thurnabilite)

    """
    # NB : pour simplifier, on considère donc que les GL sont thurnables
    # mais on les exclut des statistiques
    
    #Il semble que le dtype soit parfois perdu, d’où des bugs étranges
    listes_thurnabilite["GL"] = listes_thurnabilite["GL"].astype(bool)
    
    sans_gl = listes_thurnabilite[~listes_thurnabilite["GL"]]
    sans_gl = sans_gl[["id_normalien", "mise", "thurnable"]]
    
    sans_gl = sans_gl.groupby("id_normalien").mean()
    sans_gl.rename(columns={"thurnable": "pemp"}, inplace=True)
    sans_gl.sort_values(["mise"], ascending=[True], inplace=True)
    
    sans_gl["delta"] = abs(sans_gl["mise"] - sans_gl["pemp"])
    
    return sans_gl



reset_seed()

NB_THURNAGES = 10000

liste_probas_empiriques = []
liste_props_thurnes = []
champs_cat = ["%Or empirique", "%Argent empirique", "%Bronze empirique"]
for i, p in enumerate(liste_promos):
    print(i)
    #Ne pas tenir compte des personnes qui ne participent de toute façon pas
    #(mise nulle et pas de GL)
    p = p[(p["mise"] > 0) | p["GL"] ]
    thurnages = liste_thurnabilite_multi(p, NB_THURNES[i], NB_THURNAGES)
    pemp = pemp_thurnabilite(thurnages)
    pemp["promo"] = DUREE_INIT + i #Pour détromper par la suite
    liste_probas_empiriques.append(pemp)
    
    dict_props_thurnes = {}
    for i, cat in enumerate([OR, ARGENT, BRONZE]):
        totcat = sum(thurnages["categorie"] == cat)
        if totcat == 0:
            dict_props_thurnes[champs_cat[i]] = None
        else:
            dict_props_thurnes[champs_cat[i]] = sum((thurnages["categorie"] == cat) & (thurnages["thurnable"])) / totcat
    liste_props_thurnes.append(dict_props_thurnes)

#Toutes probas empiriques
tpe = pd.concat(liste_probas_empiriques)
tpe.sort_values(["mise"], inplace=True)



#%% Statistiques pour la deuxième partie

def stats_une_annee_empirique(pemp):
    """
    Réalise des statistiques sur la probabilité empirique de thurnage pour 
    une année donnée.
    
    Parameters
    ----------
    pemp : DataFrame contenant les probabilités empiriques, les mises et le
    delta.
        
    Returns
    -------
        Un dictionnaire contenant un certain nombre d’indicateurs sur le TG
        de l’année considérée.

    """
    inscrits = len(pemp)
    delta = pemp["delta"]
    stats = {}
    
    stats["Q1"] = delta.quantile(.25)
    stats["Med"] = delta.median()
    stats["Q3"] = delta.quantile(.75)
    stats["max"] = delta.max()
    stats["mean"] = delta.mean()
    stats["<1%"] = sum(delta < .01) / inscrits
    stats["<2%"] = sum(delta < .02) / inscrits
    stats["<5%"] = sum(delta < .05) / inscrits
    stats["<10%"] = sum(delta < .1) / inscrits
    
    return stats

stats2 = pd.DataFrame([(stats_une_annee_empirique(pemp)) for pemp in liste_probas_empiriques])
stats3 = pd.DataFrame(liste_props_thurnes)

stats = pd.concat([stats1, stats2, stats3], axis="columns")






















######################################
#%% PRODUCTION DE STATS ET GRAPHES %%#
######################################


#%% Évolution en fonction du temps de quelques variables
NB_ANNEES = 30 #Se limiter à une évolution sur 30 ans, par lisibilité
statslim = stats[:NB_ANNEES]
x = range(NB_ANNEES)

def mark_annees_penurie(stats, ax=plt):
    for i, v in enumerate(stats["cible UDG"].values):
        if v < 1:
            ax.axvspan(i-.5, i+.5, color="red", alpha=.3)

#%% Nombre de thurnes
plt.plot(NB_THURNES)
plt.plot([0,190], [270,270], "--")
plt.ylim([0,450])
plt.xlabel("Année")
plt.ylabel("Nombre thurnes")
plt.savefig(PATH + "nb_thurnes_temps.png", dpi=300)
plt.close()


#%% Logement par catégorie
# Il y aura des trous dans la courbe
or_nohole = np.nan_to_num(statslim["%Or logés"], nan=1)
ag_to_plot = or_nohole + statslim["%Argent logés"]
br_to_plot = ag_to_plot + statslim["%Bronze logés"]

plt.plot(x, statslim["%Or logés"].values * 100, "-+", color="gold", label="Cat. or")
plt.plot(x, ag_to_plot * 100, color="gray", label="Cat. argent")
plt.plot(x, br_to_plot * 100, color="darkgoldenrod", label="Cat. bronze")
plt.ylim([0,300])
plt.xlabel("Année")
plt.ylabel("Proportion cumulée (%)")
plt.legend()

mark_annees_penurie(statslim)

plt.savefig(PATH + "evol_prop_loges_1shot.png", dpi=300)
plt.close()


#%% Taux de marge dans le thurnage des or
plt.plot(x, statslim["marge or"] * 100, "-+", label="Ratio nb thurnes / taille cat. or")
margemin = min(statslim["marge or"]) * 100
plt.plot([0, NB_ANNEES], [margemin, margemin], "--", label="Valeur minimale atteinte")
plt.xlabel("Année")
plt.ylim([0,290])
plt.ylabel("%")
plt.legend()

mark_annees_penurie(statslim)

plt.savefig(PATH + "evol_marge_or.png", dpi=300)
plt.close()

print("Évolution de la marge or")
print(stats["marge or"].describe())

#%% Scatter plot pour pemp en fonction de mise

plt.scatter(tpe["mise"], 100*tpe["pemp"], s=1, alpha=.02)
# Calcul du r2
r2 = 1 - sum((tpe["pemp"] - tpe["mise"])**2) / sum((tpe["pemp"])**2)
#plt.plot([0,1],[0,100], c="red")
plt.xlabel("Mise (UDG)")
plt.ylabel("Probabilité empirique d’obtention de thurne (%)")
plt.title("R² = {:.3f}".format(r2))
plt.savefig(PATH + "scatter_mise_pemp.png", dpi=900)
plt.close()

#%% Calcul du coefficient de la régression année par année
r2_annees = [1 - sum((tpe["pemp"] - tpe["mise"])**2) / sum((tpe["pemp"])**2) for tpe in liste_probas_empiriques]
mark_annees_penurie(statslim)
plt.plot(x, r2_annees[:NB_ANNEES])
plt.plot([0,NB_ANNEES], [r2, r2], "--")
plt.xlabel("Année")
plt.ylabel("R^2")
plt.ylim([0,1])
plt.savefig(PATH + "r2_lineaire_annees.png", dpi=300)
plt.close()

#%% Courbe pemp moyenne en fonction de la mise
seuils = np.arange(.025, 1.05, .05)
mises = np.arange(0, 1.05, .05)
pemp = []
delta = []
for s in seuils:
    joueurs = tpe[(tpe["mise"] < s) & (tpe["mise"] >= s - .05) ]
    pemp.append(joueurs["pemp"].mean() * 100)
    delta.append(joueurs["delta"].mean() * 100)
plt.plot(mises, pemp, label="Probabilité empirique d’être thurné")
plt.plot([0,1], [0,100], "--", label="Cas idéal")
plt.plot(mises, delta, label="Écart moyen |mise - proba empirique|")
plt.xlabel("Mise (UDG)")
plt.ylabel("Probabilité (%)")
plt.legend()
plt.savefig(PATH + "pemp_toutes_annees_confondues.png", dpi=300)
plt.close()

#%% Courbe de répartition de delta toutes années
delta = tpe["delta"]
quantile = np.linspace(0,1,len(delta))
delta = delta.sort_values()
plt.plot(quantile, delta*100)
plt.xlabel("Quantile de normalien")
plt.ylabel("Écart moyen |mise - proba empirique| (%)")
print("Distribution de l’écart moyen")
print(delta.describe())
plt.savefig(PATH + "distribution_delta_tpe.png", dpi=300)
plt.close()

#%% Représenter la mise et la proba empirique de thurnage pour quelques années permettant d’illustrer ce qui se passe
def plot_pemp_mise(pemp, cible, nb_entrants, title):
    """
    Affiche les courbes correspondant au nombre d’UDG jouées et à la proba
    empirique de thurnabilité correspondante.
    
    Parameters
    ----------
    pemp : DataFrame
        Contient au moins une colonne mise et une colonne pemp, correspondant
        à la probabilité mepirique de thurnabilité.
    cible : cible d’UDG pour l’année
    nb_entrants : nombre d’entrants pour l’année.
    title : titre de l’image à sauvegarder

    Returns
    -------
    None.

    """
    plt.figure()
    xaxis = np.linspace(0,1,len(pemp))
    plt.plot(xaxis, np.array(pemp["mise"]), label="Mise (UDG)")
    plt.plot(xaxis, np.array(pemp["pemp"]), label="Proba empirique")
    plt.xlabel("Quantile de normalien, ordonnés par mise")
    plt.legend()
    plt.title("Cible : {:.2f} UDG ; {:d} nouveaux entrants".format(cible, nb_entrants))
    plt.savefig(PATH + title + ".png", dpi = 300)
    plt.close()

for i in [32, 56, 67, 120, 127, 132, 137, 140, 148, 170]:
    plot_pemp_mise(liste_probas_empiriques[i],
                   stats["cible UDG"][i],
                   stats["nb firsttime"][i],
                   "probemp" + str(i))

#%% Évolution de quelques paramètres dans la distribution de delta
#%% D’abord : paramètres selon l’année
plt.plot(x, statslim["Q1"]*100, color="green", label="Q1")
plt.plot(x, statslim["Med"]*100, color="darkcyan", label="Médiane")
plt.plot(x, statslim["Q3"]*100, color="blue", label="Q3")
plt.plot(x, statslim["mean"]*100, "--", color="black", label="Moyenne")
mark_annees_penurie(statslim)
plt.legend()
plt.xlabel("Année")
plt.ylabel("%")
plt.savefig(PATH + "evol_distrib_delta.png", dpi=300)
plt.close()

print("Distribution de delta")
print(stats[["Q1", "Med", "Q3", "mean"]].describe())

#%% Puis quantiles
plt.plot(x, 100*statslim["<1%"], color="green", label="Écart < 1%")
plt.plot(x, 100*statslim["<2%"], color="darkcyan", label="Écart < 2%")
plt.plot(x, 100*statslim["<5%"], color="steelblue", label="Écart < 5%")
plt.plot(x, 100*statslim["<10%"], color="blue", label="Écart < 10%")
plt.xlabel("Année")
plt.ylim([0,100])
plt.ylabel("Proportion (%)")
plt.legend()
mark_annees_penurie(statslim)
plt.savefig(PATH + "evol_quantiles_delta.png", dpi=300)
plt.close()
print("Distribution des quantiles de delta")
print(stats[["<1%", "<2%", "<5%", "<10%"]].describe())

#%% Enfin, proportion moyenne de logement par catégories
# en fonction du temps
fig, axs = plt.subplots(3)
axs[2].plot(x, 100*statslim["%Or empirique"], "-+", label="Cat. or", color="gold")
mark_annees_penurie(statslim, axs[2])
axs[1].plot(x, 100*statslim["%Argent empirique"], label="Cat. argent", color="gray")
mark_annees_penurie(statslim, axs[1])
axs[0].plot(x, 100*statslim["%Bronze empirique"], label="Cat. bronze", color="darkgoldenrod")
mark_annees_penurie(statslim, axs[0])
axs[0].legend()
axs[1].legend()
axs[2].legend()

axs[0].set_ylim([0,110])
axs[1].set_ylim([0,110])
axs[2].set_ylim([0,110])

plt.xlabel("Année")
plt.ylabel("Proportion (%)")

for ax in axs.flat:
    ax.set(xlabel='Année', ylabel='Proportion (%)')

for ax in fig.get_axes():
    ax.label_outer()

plt.savefig(PATH + "evol_prop_loges_multisimus.png", dpi=300)
plt.close()

print("Description des proportions empiriques thurnées par catégorie")
print(stats[["%Or empirique", "%Argent empirique", "%Bronze empirique"]].describe())